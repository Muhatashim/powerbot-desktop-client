package org.virus.pbdtop.ui.utils;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

//based off of AwesomeDude class by Jens Deters and from Mercury IRC Client
public class FontAwesome {

    public static final String EXIT     = "\uf05c";
    public static final String SIGN_OUT = "\uf08b";

    public static Button createIconButton(
            String iconName,
            String text,
            boolean pad,
            String styleClass,
            EventHandler<ActionEvent> eventHandler) {

        ButtonBuilder builder = ButtonBuilder.create()
                .text(text)
                .graphic(createIcon(iconName, styleClass != null))
                .contentDisplay(ContentDisplay.RIGHT)
                .focusTraversable(false)
                .onAction(eventHandler);
        if (pad) {
            builder.minHeight(33).maxHeight(33);
            if (text.equals("")) {
                builder.minWidth(33).maxWidth(33);
            }
        }
        if (styleClass != null) {
            builder.styleClass(styleClass);
            if (pad) {
                builder.minHeight(32).maxHeight(32);
            }
        }
        return builder.build();
    }

    public static Label createIcon(String iconName) {
        return createIcon(iconName, false);
    }

    public static Label createIcon(String iconName, boolean translate) {
        LabelBuilder builder = LabelBuilder.create()
                .text(iconName)
                .styleClass("icon");
        if (translate) {
            builder.translateY(3);
        }
        return builder.build();
    }

}