package org.virus.pbdtop.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextBuilder;
import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.app.SubApplication;
import org.virus.pbdtop.ui.utils.FontAwesome;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/5/13
 * Time: 10:17 AM
 */
public class AppWindow extends VBox {

    private final SubApplication forApp;

    private StackPane title;
    private boolean   titleBarOpen;

    public AppWindow(SubApplication forApp) {
        super(4);
        this.forApp = forApp;

        setPadding(new Insets(10));
    }

    public StackPane getTitleBar() {
        return title;
    }

    public SubApplication getForApp() {
        return forApp;
    }

    public void addBody() {
        getChildren().add(forApp.getUI().getContent());
    }

    public boolean hasBody() {
        return getChildren().contains(forApp.getUI().getContent());
    }

    public void removeBody() {
        getChildren().remove(forApp.getUI().getContent());
    }

    public AppWindow addTitleBar(boolean closable) {
        title = new StackPane();

        title.getStyleClass().add("titleBar");
        title.setMinHeight(32);

        title.getChildren().add(
                HBoxBuilder.create()
                        .alignment(Pos.CENTER)
                        .children(
                                TextBuilder.create()
                                        .text(forApp.getUI().getTitle())
                                        .build())
                        .build());

        if (closable)
            title.getChildren().add(HBoxBuilder.create()
                    .alignment(Pos.CENTER_RIGHT)
                    .children(
                            FontAwesome.createIconButton(
                                    FontAwesome.EXIT,
                                    "",
                                    true,
                                    "misc",
                                    new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent actionEvent) {
                                            forApp.getUI().close();
                                        }
                                    }
                            ))
                    .build());

        title.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Interface ui = getForApp().getUI();

                ui.setVisible(!ui.isVisible());
            }
        });

        getChildren().add(0, title);
        return this;
    }

    public boolean hasTitleBar() {
        return title != null && getChildren().contains(title);
    }

    public AppWindow removeTitleBar() {
        if (title != null)
            getChildren().remove(title);

        return this;
    }
}
