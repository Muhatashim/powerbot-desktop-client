package org.virus.pbdtop.ui;

import javafx.collections.ListChangeListener;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.virus.pbdtop.app.appimpls.home.Home;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:03 PM
 */
public class ApplicationsView extends VBox {

    private final Stage stage;

    public ApplicationsView(Stage stage) {
        this.stage = stage;

        setAlignment(Pos.BOTTOM_CENTER);

        getChildren().addListener(new ListChangeListener<Node>() {
            @Override
            public void onChanged(Change<? extends Node> change) {
                change.next();
                int size = change.getList().size();

                switch (size) {
                    default:
                        if (change.wasAdded()) {
                            List<? extends Node> addedSubList = change.getAddedSubList();

                            for (Node node : getChildren())
                                if (node instanceof AppWindow) {
                                    AppWindow appWindow = ((AppWindow) node);

                                    if (!appWindow.hasTitleBar())
                                        appWindow.addTitleBar(!(appWindow.getForApp() instanceof Home));

                                    appWindow.getTitleBar().setId(
                                            addedSubList.contains(appWindow) || appWindow.hasBody()
                                                    ? "open" : "minimized");
                                }
                        }
                        break;
                    case 0:
                        final Text noAppText = new Text("No apps");

                        getChildren().add(noAppText);
                        getChildren().addListener(new ListChangeListener<Node>() {
                            @Override
                            public void onChanged(Change<? extends Node> change) {
                                if (change.wasAdded()) {
                                    getChildren().remove(noAppText);
                                    getChildren().removeListener(this);
                                }
                            }
                        });
                        break;
                    case 1:
                        Node node = getChildren().get(0);
                        if (node instanceof AppWindow)
                            ((AppWindow) node).removeTitleBar().getForApp().getUI().show();
                        break;
                }
            }
        });
    }

    public Stage getStage() {
        return stage;
    }

    public void remove(AppWindow ui) {
        getChildren().remove(ui);

    }

    public void add(AppWindow ui) {
        if (!getChildren().contains(ui))
            getChildren().add(0, ui);
    }
}
