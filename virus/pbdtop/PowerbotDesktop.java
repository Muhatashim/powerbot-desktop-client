package org.virus.pbdtop;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.virus.pbdtop.app.appimpls.forums.Forums;
import org.virus.pbdtop.app.appimpls.home.Home;
import org.virus.pbdtop.app.appimpls.home.HomeInterface;
import org.virus.pbdtop.app.appimpls.irc.IRC;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:01 PM
 */
public class PowerbotDesktop extends Application {

    public static final String FONT_FONT_AWESOME     = "font_awesome";
    public static final String FONT_FONT_INCONSOLATA = "inconsolata";

    public static void main(String[] args) {
        Font.loadFont(PowerbotDesktop.class.getResourceAsStream("./res/fonts/font_awesome.ttf"), 12);
        Font.loadFont(PowerbotDesktop.class.getResourceAsStream("./res/fonts/inconsolata.ttf"), 12);

        Application.launch(PowerbotDesktop.class);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ApplicationsView appView = new ApplicationsView(stage);
        Scene scene = new Scene(appView);

        scene.getStylesheets().add(PowerbotDesktop.class.getResource("./res/css/Metro.css").toExternalForm());

        stage.setTitle("powerbot Desktop Client");
        stage.setWidth(720);
        stage.setHeight(480);
        stage.setScene(scene);
        stage.show();

        HomeInterface homeUI = (HomeInterface) new Home(appView).getUI();

        homeUI.show();
        homeUI.addApp(new Forums(appView));
        homeUI.addApp(new IRC(appView));
    }
}
