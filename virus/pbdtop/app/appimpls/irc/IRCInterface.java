package org.virus.pbdtop.app.appimpls.irc;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextAreaBuilder;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFieldBuilder;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBoxBuilder;
import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:11 PM
 */
public class IRCInterface extends Interface<IRC> {

    private final TextArea  chatArea;
    private final TextField textToSend;

    public IRCInterface(ApplicationsView appViewer, final IRC app) {
        super(appViewer, app);

        setContent(VBoxBuilder.create()
                .children(
                        HBoxBuilder.create()
                                .spacing(5)
                                .children(
                                        chatArea = TextAreaBuilder.create()
                                                .minHeight(20)
                                                .wrapText(true)
                                                .editable(false)
                                                .build(),

                                        new OnlineUsers())
                                .alignment(Pos.CENTER)
                                .build(),
                        textToSend = TextFieldBuilder.create()
                                .minHeight(20)
                                .promptText("Hello!")
                                .onKeyReleased(new EventHandler<KeyEvent>() {
                                    @Override
                                    public void handle(KeyEvent keyEvent) {
                                        app.handleKeyReleased(keyEvent);
                                    }
                                })
                                .build())
                .spacing(6)
                .build());

        HBox.setHgrow(chatArea, Priority.ALWAYS);
    }

    public TextArea getChatArea() {
        return chatArea;
    }

    public TextField getTextToSend() {
        return textToSend;
    }

    @Override
    protected Image loadImage() {
        return new Image("http://thumb18.shutterstock.com/photos/thumb_large/305872/305872,1238324445,1.jpg");
    }
}
