package org.virus.pbdtop.app.appimpls.irc;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ListViewBuilder;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.VBox;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/5/13
 * Time: 5:22 PM
 */
public class OnlineUsers extends VBox {

    private final ListView<String> usersOnline;

    public OnlineUsers() {
        getChildren().add(HBoxBuilder.create()
                .children(
                        new Label("Online Users"))
                .alignment(Pos.CENTER)
                .build());
        getChildren().add(usersOnline = ListViewBuilder.<String>create()
                .maxWidth(100)
                .build());
    }

    public ListView<String> getUsersOnline() {
        return usersOnline;
    }
}
