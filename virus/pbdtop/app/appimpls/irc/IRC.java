package org.virus.pbdtop.app.appimpls.irc;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.app.SubApplication;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:11 PM
 */
public class IRC implements SubApplication {

    private final IRCInterface ui;

    public IRC(ApplicationsView appViewer) {
        ui = new IRCInterface(appViewer, this);
        ui.setTitle(getName());
    }

    @Override
    public Interface getUI() {
        return ui;
    }

    @Override
    public String getName() {
        return "IRC";
    }

    public void handleKeyReleased(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER)
            sendInput();
    }

    private void sendInput() {
        String text = ui.getTextToSend().getText();
        ui.getTextToSend().setText("");
        System.out.println(text);
    }
}
