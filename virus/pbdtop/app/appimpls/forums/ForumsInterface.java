package org.virus.pbdtop.app.appimpls.forums;

import javafx.scene.image.Image;
import javafx.scene.layout.StackPaneBuilder;
import javafx.scene.text.Text;
import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 5:18 PM
 */
public class ForumsInterface extends Interface<Forums> {
    public ForumsInterface(ApplicationsView appViewer, Forums app) {
        super(appViewer, app);

        setContent(StackPaneBuilder.create()
                .children(
                        new Text("Under Construction"))
                .build());
    }

    @Override
    protected Image loadImage() {
        return new Image("http://png-3.findicons.com/files/icons/2117/nuove/128/kontact_contacts.png");
    }
}
