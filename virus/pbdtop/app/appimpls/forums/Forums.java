package org.virus.pbdtop.app.appimpls.forums;

import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.app.SubApplication;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:11 PM
 */
public class Forums implements SubApplication {

    private final ForumsInterface ui;

    public Forums(ApplicationsView appViewer) {
        ui = new ForumsInterface(appViewer, this);
        ui.setTitle(getName());
    }

    @Override
    public Interface getUI() {
        return ui;
    }

    @Override
    public String getName() {
        return "Forums";
    }
}
