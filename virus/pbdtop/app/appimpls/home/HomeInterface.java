package org.virus.pbdtop.app.appimpls.home;

import javafx.beans.binding.ObjectBinding;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.app.SubApplication;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 9:05 PM
 */
public class HomeInterface extends Interface<Home> {
    private final FlowPane         ui;
    private final ImageViewBuilder iconBuilder;

    public HomeInterface(ApplicationsView appViewer, Home app) {
        super(appViewer, app);

        ui = new FlowPane();

        ui.setVgap(25);
        ui.setHgap(25);
        ui.prefWrapLengthProperty().bind(new ObjectBinding<Number>() {
            @Override
            protected Number computeValue() {
                return getAppViewer().getWidth();
            }
        });
        ui.setAlignment(Pos.CENTER);
        ui.setPadding(new Insets(20));

        iconBuilder = ImageViewBuilder.create();
        iconBuilder
                .fitHeight(100)
                .fitWidth(100);


        setContent(ui);
    }

    public void addApp(final SubApplication application) {
        final ImageView build = iconBuilder
                .image(application.getUI().getIcon())
                .build();

        build.setEffect(new DropShadow(
                15,
                5, 5,
                Color.BLACK));

        build.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        forApp.mouseClickedApp(application, build);
                    }
                }
        );

        ui.getChildren().add(build);
    }

    @Override
    protected Image loadImage() {
        return new Image("http://haroldallen.com/images/site_graphics/home.png");
    }
}
