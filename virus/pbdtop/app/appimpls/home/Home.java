package org.virus.pbdtop.app.appimpls.home;

import javafx.scene.image.ImageView;
import org.virus.pbdtop.app.Interface;
import org.virus.pbdtop.app.SubApplication;
import org.virus.pbdtop.ui.ApplicationsView;


/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 9:02 PM
 */
public class Home implements SubApplication {

    private final HomeInterface ui;

    public Home(ApplicationsView appView) {
        ui = new HomeInterface(appView, this);
        ui.setTitle(getName());
    }

    protected void mouseClickedApp(SubApplication application, ImageView appIcon){
        appIcon.setScaleX(1.3);
        appIcon.setScaleY(1.3);

        application.getUI().show();
    }

    @Override
    public Interface getUI() {
        return ui;
    }

    @Override
    public String getName() {
        return "Home";
    }
}
