package org.virus.pbdtop.app;


/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:04 PM
 */
public interface SubApplication {

    Interface getUI();

    String getName();
}
