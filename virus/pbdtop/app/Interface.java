package org.virus.pbdtop.app;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import org.virus.pbdtop.ui.AppWindow;
import org.virus.pbdtop.ui.ApplicationsView;

/**
 * Created with IntelliJ IDEA.
 * User: muhatashim
 * Date: 8/4/13
 * Time: 4:07 PM
 */
public abstract class Interface<T extends SubApplication> {

    private final   ApplicationsView appViewer;
    protected final T                forApp;

    private AppWindow uiWindow;
    private Pane      ui;
    private Image     icon;
    private String    title;

    public Interface(ApplicationsView appViewer, T forApp) {
        this.appViewer = appViewer;
        this.forApp = forApp;
        uiWindow = new AppWindow(forApp);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public final Image getIcon() {
        if (icon == null)
            return icon = loadImage();

        return icon;
    }

    protected abstract Image loadImage();

    protected final void setContent(Pane pane) {
        ui = pane;
        ui.getStyleClass().add("appInterface");
    }

    public final Pane getContent() {
        return ui;
    }

    public final ApplicationsView getAppViewer() {
        return appViewer;
    }

    public final boolean isVisible() {
        return uiWindow.hasBody();
    }

    public final void setVisible(boolean visible) {
        if (visible)
            show();
        else
            hide();
    }

    public final void show() {
        appViewer.add(uiWindow);
        if (!uiWindow.hasBody())
            uiWindow.addBody();

        if (uiWindow.getTitleBar() != null)
            uiWindow.getTitleBar().setId("open");
    }

    public final void hide() {
        if (uiWindow.hasBody())
            uiWindow.removeBody();

        if (uiWindow.getTitleBar() != null)
            uiWindow.getTitleBar().setId("minimized");
    }

    public void close() {
        appViewer.remove(uiWindow);
    }
}
